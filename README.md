## Simple Odoo module

### Steps to reproduce development env

1. Start the container via: `docker-compose up`
2. Go to `localhost:8069` and create database, master key is in config file.
3. On the web-site navigate to Apps, remove filter and start typing `my...`.
4. Choose application module named **My module**, click on `Install`.
5. Changes provided via this module can be seen in the employee subsite.

### Tasks

1. (easy) Create a new Boolean field on the specific screen after specific field.
2. (medium) Create 3 new Integer fields, the value of the third depends on values of the first and second field.
3. (advanced) Insert new Binary field under the field created in task 1, show it only if the Boolean field is checked.
   - create an excel with 2+ lines, first column should be some email and second just random string.
   - create new button 'Send emails' right next to the button 'Launch plan'
   - after clicking on this button an email should be send based on the data from the uploaded excel file
