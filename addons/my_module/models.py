import base64
import logging

from odoo import models, fields, api
import xlrd


logging.basicConfig(level=logging.INFO)


class HrEmployeeInherit(models.Model):
    _inherit = 'hr.employee'

    # ------------
    # zadanie c. 1
    i_love_ddls = fields.Boolean(string='I love Dedoles')
    # ------------


    # ------------
    # zadanie c. 2
    currency_id = fields.Many2one(
        'res.currency', 'Currency',
        default=lambda self: self.env.user.company_id.currency_id.id,
        required=True)
    salary = fields.Monetary(string="Salary")
    tax = fields.Monetary(string="Tax")
    total_salary = fields.Monetary(string="Total Salary")

    @api.onchange('salary', 'tax')
    def _onchange_salary_tax(self):
        self.total_salary = self.salary + self.tax

    # total_salary = fields.Float(compute='_compute_salary')
    # @api.depends('salary', 'tax')
    # def _compute_salary(self):
    #     self.total_salary = self.salary + self.tax
    # ------------


    # ------------
    # zadanie c. 3
    employee_contacts = fields.Binary(attachment=False)

    def send_emails(self):
        book = xlrd.open_workbook(
            file_contents=base64.decodebytes(self.employee_contacts))
        sh = book.sheet_by_index(0)
        for rx in range(sh.nrows):
            row = sh.row(rx)
            mail_obj = self.env['mail.mail']

            try:
                mail = mail_obj.create({
                    'email_to': row[0].value,
                    'subject': row[1].value,
                    'body': 'Welcome in Dedoles',
                })
                mail.send()
                logging.info(f'Email sent to user: {row[0].value}')
            except:
                logging.error(f'Email failed to by sent to: {row[1].value}')
    # failed emails: Settings -> Technical -> Emails
    # ------------
